# Vim Settings

The basic idea behind vim-settings is to sync Vim settings in a git repository using [vim-plug](https://github.com/junegunn/vim-plug).
The solution is built with my settings embedded for others to use or fork and customize.
Once setup, any customizations and updates are synced via vim-plug.
In addition, as people fork the repo we can all take from each other and have a nice place to see what others are doing.

### Install
- If you want to customize the settings then fork this repository first
- Create/replace your `.vimrc` file, updating `settings_plugin` to your vim-settings plugin if you forked the repository
    - Open Vim and run `:e $MYVIMRC` to get the destination
    - You can use the following command to automatically install an existing version
        - `wget -O ~/.vimrc https://gitlab.com/flippidippi/vim-settings/raw/master/.vimrc`
- Open Vim and it should install everything automatically

### Make Update
- You will need to have forked vim-settings to do this
- Make changes to vim-settings
    - You can open the vim-settings settings file with `<leader>rc`
- Run `<leader>rC` to refresh with any changes you make
- If you add/remove plugins run `:PlugInstall` or `:PlugClean` respectively, and `<leader>rC` again to refresh
- Commit and push the changes via git
    - Use [vim-fugitive](https://github.com/tpope/vim-fugitive) for quicker results
    - If you need to use ssh run `:Git remote set-url origin git@gitlab.com:[USERNAME]/vim-settings.git`

### Pull Update
- Run `:PlugUpdate`
- Run `<leader>rC` to refresh with changes
- If new plugins are added to then run `:PlugInstall`, and `<leader>rC`.
