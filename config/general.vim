" Remove escape lag
set timeoutlen=1000 ttimeoutlen=0

" Variables can be set per file
set modeline

" Show line numbers
set number

" Highlights matching braces
set showmatch

" Search starts while entering string
set incsearch

" Search highlighting
set hlsearch

" Search ignore case
set ignorecase

" Search ignore case unless search contains an uppercase
set smartcase

" Auto-load changes to file made outside Vim
set autoread

" Do not wrap lines
set nowrap

" Prevent automatically inserting line breaks
set textwidth=0

" Command line completion help
set wildmenu

" Extensions to ignore in searches
set wildignore+=*.o,*.~,*.pyc,*.exe,*/tmp/*,*.so,*.swp,*.zip,*\\tmp\\*

" Turn off folding by default
set nofoldenable

" Fold by syntax highlighting
set foldmethod=syntax

" Minimum number of screen rows above and below the cursor
set scrolloff=4

" Minimum number of screen columns to the left and right of the cursor
set sidescrolloff=0

" Always show the status line
set laststatus=2

" Enable mouse in all modes
set mouse=a

" Allow use of system clipboard
set clipboard=unnamed

" Buffer screen updates to speed up macros etc.
set lazyredraw

" Set column to auto show as needed
set signcolumn=auto

" Disable message at start
set shortmess+=Ic

" New window appears below current
set splitbelow

" New window appears to the right of current
set splitright

" Set character representation via UTF-8
silent! set encoding=utf8

" File type preferences
set fileformats=unix,dos

" Don't create backups
set nobackup
set nowritebackup

" Turn off swap files
set noswapfile

" Buffer becomes hidden when abandoned to prevent need to save
set hidden

" Enable file type detection/plugin/indent
filetype plugin indent on

" Set autocomplete to syntax completion
set omnifunc=syntaxcomplete#Complete

" Set searching to global by default
set gdefault

" Make backspace behave
set backspace=indent,eol,start

" Set spell check
set spelllang=en
let &spellfile=$SETTINGS_PATH . '/spell/en.utf-8.add'

" Set update time
set updatetime=300

" Automatically indent properly
set autoindent

" Text is indented by 2 columns
set shiftwidth=2

" Insert spaces when tab pressed
set expandtab

" Insert 2 columns when tab pressed
set softtabstop=2

" Set tab width to 2
set tabstop=2

" Conceal markers
set conceallevel=0
