" File type settings
augroup FileTypeSettings
  autocmd!

  " RVM .ruby-env
  autocmd BufNewFile,BufReadPost *.ruby-env setlocal filetype=sh

  " JSON RC files
  autocmd BufNewFile,BufReadPost .babelrc,.stylelintrc setlocal filetype=json

  " Conf files
  autocmd BufNewFile,BufReadPost .dockerignore setlocal filetype=conf
augroup END

" React in Javascript files
augroup JavascriptLibraries
  autocmd!
  autocmd BufReadPre *.js let b:javascript_lib_use_react=1
augroup END

" Fix for reloading files when changed outside Neovim
augroup ReloadFiles
  autocmd!
  autocmd BufEnter,FocusGained * checktime
augroup END

" Git gutter colors
augroup GitGutterColors
  autocmd!
  highlight! link GitGutterChange GruvboxYellowSign
  highlight! link GitGutterChangeDelete GruvboxPurpleSign
augroup END

" Enter insert mode when switch to terminal
augroup TerminalStart
  autocmd!
  autocmd BufEnter term://* startinsert
  autocmd TerminalOpen * setlocal nobuflisted
augroup END
