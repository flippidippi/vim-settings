" Font
set guifont=Hack_NF:h10:cANSI:qDRAFT

" Enable full color
set termguicolors

" Color scheme
silent! colorscheme gruvbox
set background=dark
if $ITERM_PROFILE == 'light'
  set background=light
endif
let g:gruvbox_contrast_dark='hard'
let g:gruvbox_contrast_light='hard'
let g:gruvbox_italic=1
let g:gruvbox_italicize_strings=1
let g:gruvbox_sign_column='bg0'
let g:gitgutter_override_sign_column_highlight=0

" Enable syntax highlighting
syntax on

" Hide scrollbars
set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=L

" Hide menu bar
set guioptions-=m

" Hide toolbar
set guioptions-=T

" Windows terminal cursor
if &term =~ '^xterm'
  " Normal mode
  let &t_EI .= "\<Esc>[0 q"
  " Insert mode
  let &t_SI .= "\<Esc>[6 q"
endif
