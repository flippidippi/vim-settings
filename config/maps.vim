" Set map leader to space
let mapleader=' '

" Edit settings
nnoremap <leader>rc :e $SETTINGS_FILE<cr>

" Source Vim RC
nnoremap <leader>rC :so $MYVIMRC<cr>

" Visual mode replace without copy
vmap r "_dP

" Git status
nnoremap <leader>gs :Git<cr>

" Git diff
nnoremap <leader>gd :Gdiff<cr>

" Markdown shortcut for table format
nnoremap <leader>ft :TableFormat<cr>

" Coc do hover
nnoremap <silent> K :call CocAction('doHover')<cr>

" Coc go to definition
nmap <silent> gd <plug>(coc-definition)

" Coc go to implementation
nmap <silent> gi <plug>(coc-implementation)

" Coc go to references
nmap <silent> gr <plug>(coc-references)

" Coc object rename
nmap <leader>rn <plug>(coc-rename)

" Coc pick color
nnoremap <leader>fc :call CocAction('pickColor')<cr>

" Coc fix autofix problem of current line
nmap <leader>fq <plug>(coc-fix-current)

" Coc format
vmap <leader>= <plug>(coc-format-selected)
nmap <leader>= <plug>(coc-format-selected)

" Run jest for current project
nnoremap <leader>jj :call CocAction('runCommand', 'jest.projectTest')<cr>

" Run jest for current file
nnoremap <leader>jf :call CocAction('runCommand', 'jest.fileTest', ['%'])<cr>

" Run jest for current test
nnoremap <leader>jt :call CocAction('runCommand', 'jest.singleTest')<cr>

" Coc tab completion/snippet
inoremap <silent><expr> <tab> 
  \ pumvisible() ? '<c-n>' : 
  \ coc#expandableOrJumpable() ? "\<c-r>=coc#rpc#request('doKeymap', ['snippets-expand-jump',''])\<CR>" :
  \ CheckBackSpace() ? '<tab>' 
  \ : coc#refresh()

" Coc shift-tab completion/delimitMate jump
imap <expr> <s-tab> pumvisible() ? '<c-p>' : "<plug>delimitMateS-Tab"

" Coc enter to complete
imap <expr> <cr> pumvisible() ? '<c-y>' : "<plug>delimitMateCR"

" Coc go to object list
nnoremap <silent> fo :<c-u>CocList outline<cr>

" Coc action list
nnoremap <leader>fa :CocList actions<cr>

" Coc find file
nnoremap <leader>ff :CocList files<cr>

" Coc find recent file
nnoremap <leader>fh :CocList mru<cr>

" Coc find buffer
nnoremap <leader>fb :CocList buffers<cr>

" Coc go to object list
nnoremap <leader>fo :<c-u>CocList outline<cr>

" Coc find diagnostic
nnoremap <leader>fd :CocList diagnostics<cr>

" Coc grep
nnoremap <leader>fg :CocList grep<space>

" Coc grep for FIXME/TODO
nnoremap <leader>ft :CocList grep todo\|fixme<cr>

" Coc grep word
nnoremap <silent> <leader>fw  :exe 'CocList grep '.expand('<cword>')<cr>

" Coc resume list
nnoremap <leader>fl :CocListResume<cr>

" Shortcuts for CocNext and CocPrevious
nnoremap gn :CocNext<cr>
nnoremap gN :CocPrev<cr>

" Coc explorer toggle
nnoremap <leader>n :CocCommand explorer<cr>

" Vim over shortcut
nnoremap <leader>/ :OverCommandLine<cr>%s/

" New buffer in current area
nnoremap <leader>ee :enew<cr>

" New buffer in split
nnoremap <leader>es :new<cr>

" New buffer in vertical split
nnoremap <leader>ev :vnew<cr>

" Go to next buffer
nnoremap <tab> :bnext<cr>

" Go to previous buffer
nnoremap <s-tab> :bprevious<cr>

" Remove the current buffer and close current window
nnoremap <leader>bD :Sayonara<cr>

" Remove the current buffer and preserve current window
nnoremap <leader>bd :Sayonara!<cr>

" Remove all buffers except for the current one
nnoremap <leader>bo :BufOnly<cr>

" Split current file
nnoremap <leader>bs :split<cr>

" Vertical split current file
nnoremap <leader>bv :vsplit<cr>

" Create new tab
nnoremap <leader>tt :tabnew<cr>

" Go to next tab
nnoremap gt :tabnext<cr>

" Go to previous tab
nnoremap gT :tabprevious<cr>

" Update current buffer
nnoremap <leader>u :update<cr>

" Update all buffers
nnoremap <leader>U :wa<cr>

" Go to split below
nnoremap <c-j> <c-w>j
tnoremap <c-j> <c-\><c-n> <c-w>j

" Go to split above
nnoremap <c-k> <c-w>k
tnoremap <c-k> <c-\><c-n><c-w>k

" Go to split left, add iTerm2 Keys `^h` to send escape sequence `[104;5u`
nnoremap <c-h> <c-w>h
tnoremap <c-h> <c-\><c-n><c-w>h

" Go to split right
nnoremap <c-l> <c-w>l
tnoremap <c-l> <c-\><c-n><c-w>l

" Make splits equal
nnoremap <c-g> <c-w>=

" Make H and L go to beginning and end of line
nnoremap H _
nnoremap L g_
xnoremap H _
xnoremap L g_

" Keep search matches in the middle of the window
nnoremap n nzzzv
nnoremap N Nzzzv

" No arrow keys for you
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

" Turn off search highlight
nnoremap ? :noh<cr>

" Toggle spell check
nnoremap <leader>sc :setlocal spell!<cr>

" Sort lines alphabetically
nnoremap <leader>sl :sort i<cr>
xnoremap <leader>sl :sort i<cr>

" Sort lines numerically
nnoremap <leader>sn :sort n<cr>
xnoremap <leader>sn :sort n<cr>

" Sort lines randomly
nnoremap <leader>sr :!sort -R<cr>
xnoremap <leader>sr :!sort -R<cr>

" Better indentation
nnoremap > >>
nnoremap < <<
xnoremap > >gv
xnoremap < <gv

" Space indentation
nnoremap <leader>i<space> :call Ispaces()<cr>

" Tab indentation
nnoremap <leader>i<tab> :call Itabs()<cr>

" Toggle wrap
nnoremap <leader>wr :set invwrap<cr>:set wrap?<cr>

" Turn off ex mode
nnoremap Q <nop>

" Escape to exit terminal mode
tnoremap <esc> <c-\><c-n>

" Remove trailing whitespace
nnoremap <leader>xt :call StripTrailingWhitespace()<cr>

" Open via Markdown preview
nnoremap <leader>om :MarkdownPreview<cr>

" Open with specified program
nnoremap <leader>ow :OpenWith<space>

" Open with default program
nnoremap <leader>oo :OpenWith Finder<cr>
